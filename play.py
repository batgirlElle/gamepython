import random, sys, time, pygame
from pygame.locals import *

FPS = 42

WINDOWWIDTH = 700
WINDOWHEIGHT = 490
FLASHSPEED = 480 # milliseconds
FLASHDELAY = 150 # milliseconds
BUTTONSIZE = 190
BUTTONGAPSIZE = 25
TIMEOUT = 5 # num of seconds before game over if no button is pushed

#         R    G    B
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
BRIGHTORANGE = (253, 95, 0)
ORANGE = (255, 165, 0)
BRIGHTPINK = (255, 0, 127)
PINK = (255, 105, 180)
BRIGHTBLUE = (  0,   0, 255)
BLUE = (  0,   0, 155)
BRIGHTYELLOW = (215, 195, 42)
YELLOW = (255, 255,   0)
DARKGRAY = ( 40,  40,  40)
bgColor = BLACK

XMARGIN = int((WINDOWWIDTH - (2 * BUTTONSIZE) - BUTTONGAPSIZE) / 2)
YMARGIN = int((WINDOWHEIGHT - (2 * BUTTONSIZE) - BUTTONGAPSIZE) / 2)

# Rec objects for each of the four buttons
YELLOWREC = pygame.Rect(XMARGIN, YMARGIN, BUTTONSIZE, BUTTONSIZE)
BLUEREC = pygame.Rect(XMARGIN + BUTTONSIZE + BUTTONGAPSIZE, YMARGIN, BUTTONSIZE, BUTTONSIZE)
ORANGEREC = pygame.Rect(XMARGIN, YMARGIN + BUTTONSIZE + BUTTONGAPSIZE, BUTTONSIZE, BUTTONSIZE)
PINKREC = pygame.Rect(XMARGIN + BUTTONSIZE + BUTTONGAPSIZE, YMARGIN + BUTTONSIZE + BUTTONGAPSIZE, BUTTONSIZE, BUTTONSIZE)

def main ():
    global FPSCLOCK, DISPLAYSURF, BASICFONT, BEEP1, BEEP2, BEEP3, BEEP4

    pygame.init()
    FPSCLOCK = pygame.time.Clock()
    DISPLAYSURF = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT))
    pygame.display.set_caption('Pattern-ator')

    BASICFONT = pygame.font.Font('freesansbold.ttf', 19)
    infoSurf = BASICFONT.render('Match the pattern by pressing on the button or using the Q, W, A, S keys.', 1, WHITE)
    infoRect = infoSurf.get_rect()
    infoRect.topleft = (10, WINDOWHEIGHT - 25)

    # load the sound files
    BEEP1 = pygame.mixer.Sound('beep1.ogg')
    BEEP2 = pygame.mixer.Sound('beep2.ogg')
    BEEP3 = pygame.mixer.Sound('beep3.ogg')
    BEEP4 = pygame.mixer.Sound('beep4.ogg')

    # Initialized variables for a new game
    pattern = [] # stores pattern of colors
    currentStep = 0 # the color the player must click next
    lastClickTime = 0 # timestamp of the player's last button click

    score = 0
    # when False, the pattern is playing. When True, waiting for player to click a colored button:

    waitingForInput = False
	
    # DRAWING THE BOARD
    while True: # loop for main game
        clickedButton = None # button that was clicked (set to YELLOW, RED, GREEN, or BLUE)
        DISPLAYSURF.fill(bgColor)
        drawButtons()

        scoreSurf = BASICFONT.render('Score: ' + str(score), 1, WHITE)
        scoreRect = scoreSurf.get_rect()
        scoreRect.topleft = (WINDOWWIDTH - 100, 10)
        DISPLAYSURF.blit(scoreSurf, scoreRect)
        
        DISPLAYSURF.blit(infoSurf, infoRect)

        checkForQuit()
        for event in pygame.event.get(): # event handling loop
            if event.type == MOUSEBUTTONUP:
                mousex, mousey = event.pos
                clickedButton = getButtonClicked (mousex, mousey)
            elif event.type == KEYDOWN:
                if event.key == K_q:
                    clickedButton = YELLOW
                if event.key == K_w:
                    clickedButton = BLUE
                if event.key == K_a:
                    clickedButton = ORANGE
                if event.key == K_s:
                    clickedButton = PINK

        if not waitingForInput:
            # play pattern
            pygame.display.update()
            pygame.time.wait(900)
            pattern.append (random.choice ((YELLOW, BLUE, ORANGE, PINK)))
            
            for button in pattern:
                flashButtonAnimation(button)
                pygame.time.wait(FLASHDELAY)
            waitingForInput = True
        else:
            # wait for player to enter buttons
            if clickedButton and clickedButton == pattern[currentStep]:
                # player pushed correct button
                flashButtonAnimation (clickedButton)
                currentStep += 1
                lastClickTime = time.time()

                if currentStep == len(pattern):
                    # pushed the last button in the pattern
                    changeBackgroundAnimation()
                    score += 1
                    waitingForInput = False
                    currentStep = 0 # reset back to first step

            elif (clickedButton and clickedButton != pattern[currentStep]) or (currentStep != 0 and time.time() - TIMEOUT > lastClickTime):
                # pushed the incorrect button, or has timed out
                gameOverAnimation()
                # reset the variables for a new game:
                pattern = []
                currentStep = 0
                waitingForInput = False
                score = 0
                pygame.time.wait(900)
                changeBackgroundAnimation()

        pygame.display.update()
        FPSCLOCK.tick (FPS)


def terminate():
    pygame.quit()
    sys.exit()

def checkForQuit():
    for event in pygame.event.get (QUIT): # get all quit events
        terminate() # terminate if any quit events are present

    for event in pygame.event.get (KEYUP): #get all keyup events
        if event.key == K_ESCAPE:
            terminate() # terminate if keyup event was for Esc key
        pygame.event.post (event) # put other keyup event objects back

def flashButtonAnimation (color, animationSpeed=48):
    if color == YELLOW:
        sound = BEEP1
        flashColor = BRIGHTYELLOW
        rectangle = YELLOWREC
    elif color == BLUE:
        sound = BEEP2
        flashColor = BRIGHTBLUE
        rectangle = BLUEREC
    elif color == ORANGE:
        sound = BEEP3
        flashColor = BRIGHTORANGE
        rectangle = ORANGEREC
    elif color == PINK:
        sound = BEEP4
        flashColor = BRIGHTPINK
        rectangle = PINKREC

    origSurf = DISPLAYSURF.copy()
    flashSurf = pygame.Surface ((BUTTONSIZE, BUTTONSIZE))
    r, g, b = flashColor
    sound.play()
    
    for start, end, step in ((0, 255, 1), (255, 0, -1)): # animation loop
        for alpha in range(start, end, animationSpeed * step):
            checkForQuit()
            DISPLAYSURF.blit(origSurf, (0, 0))
            flashSurf.fill((r, g, b, alpha))
            DISPLAYSURF.blit(flashSurf, rectangle.topleft)
            pygame.display.update()
            FPSCLOCK.tick(FPS)
    DISPLAYSURF.blit(origSurf, (0, 0))


def drawButtons():
    pygame.draw.rect(DISPLAYSURF, YELLOW, YELLOWREC)
    pygame.draw.rect(DISPLAYSURF, BLUE, BLUEREC)
    pygame.draw.rect(DISPLAYSURF, ORANGE, ORANGEREC)
    pygame.draw.rect(DISPLAYSURF, PINK, PINKREC)

def changeBackgroundAnimation(animationSpeed = 42):
    global bgColor
    newBgColor = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))

    newBgSurf = pygame.Surface((WINDOWWIDTH, WINDOWHEIGHT))
    newBgSurf = newBgSurf.convert_alpha()
    r, g, b = newBgColor

    for alpha in range(0, 255, animationSpeed): # animation loop
        checkForQuit()
        DISPLAYSURF.fill(bgColor)

        newBgSurf.fill((r, g, b, alpha))
        DISPLAYSURF.blit(newBgSurf, (0, 0))

        drawButtons() # redraw buttons on top of tint

        pygame.display.update()
        FPSCLOCK.tick(FPS)
    bgColor = newBgColor

def gameOverAnimation(color=WHITE, animationSpeed=50):
     # play all beeps at once, then background flashes
     origSurf = DISPLAYSURF.copy()
     flashSurf = pygame.Surface(DISPLAYSURF.get_size())
     flashSurf = flashSurf.convert_alpha()

     # play all beep sounds at the same time
     BEEP1.play()
     BEEP2.play()
     BEEP3.play()
     BEEP4.play()
     r, g, b = color

     for i in range(3): # do the flash 3 times
         for start, end, step in ((0, 255, 1), (255, 0, -1)):
             # The first iteration in this loop sets the following for loop
             # to go from 0 to 255, the second from 255 to 0.
             for alpha in range(start, end, animationSpeed * step): # animation loop
                 # alpha means transparency. 255 is opaque, 0 is invisible
                 checkForQuit()
                 flashSurf.fill((r, g, b, alpha))
                 DISPLAYSURF.blit(origSurf, (0, 0))
                 DISPLAYSURF.blit(flashSurf, (0, 0))
                 drawButtons()
                 pygame.display.update()
                 FPSCLOCK.tick(FPS)

def getButtonClicked (x, y):
    if YELLOWREC.collidepoint((x, y)):
        return YELLOW
    elif BLUEREC.collidepoint((x, y)):
        return BLUE
    elif ORANGEREC.collidepoint((x, y)):
        return ORANGE
    elif PINKREC.collidepoint((x, y)):
        return PINK
    return None


if __name__ == '__main__':
    main()





